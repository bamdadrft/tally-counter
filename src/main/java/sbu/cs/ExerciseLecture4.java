package sbu.cs;

import java.util.Locale;

public class ExerciseLecture4 {

    public long factorial(int n)
    {
        if (n == 1)
        {
            return 1;
        }
        return n * factorial(n-1);
    }

    public long fibonacci(int n) {
        if(n == 1)
        {
            return 1;
        }
        if(n == 2)
        {
            return 1;
        }
        return fibonacci(n-1) + fibonacci(n-2);
    }

    public String reverse(String word)
    {
        return new StringBuilder(word).reverse().toString();
    }

    public boolean isPalindrome(String line)
    {
        line = line.toLowerCase(Locale.ROOT);
        StringBuilder templine = new StringBuilder(line);

        for(int i = 0; i < templine.length(); i++)
        {
            if(templine.charAt(i) == ' ')
            {
                templine.deleteCharAt(i);
            }
        }

        for(int i = 0; i < templine.length()/2; i++)
        {
            if(templine.charAt(i) != templine.charAt(templine.length()-(i+1)))
            {
                return false;
            }
        }
        return true;
    }

    /*
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and ali is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     */
    public char[][] dotPlot(String str1, String str2)
    {
        char[][] dots = new char[str1.length()][str2.length()];
        for(int i = 0; i < str1.length(); i++)
        {
            for(int j = 0; j < str2.length(); j++)
            {
                if(str1.charAt(i) == str2.charAt(j))
                {
                    dots[i][j] = '*';
                }
                else
                    dots[i][j] = ' ';
            }
        }
        return dots;
    }
}
