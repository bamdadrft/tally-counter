package sbu.cs;

import java.util.Random;

public class ExerciseLecture5 {

    public String weakPassword(int length)
    {
        Random rand = new Random();
        String password = "";
        StringBuilder pass = new StringBuilder();

        for(int i = 0; i < length; i++)
        {
            int x = rand.nextInt(26) + 97;
            pass.append((char)x);
        }
        password = pass.toString();
        return password;
    }
    
    public String strongPassword(int length) throws Exception
    {
        if(length < 3)
        {
            throw new IllegalValueException();
        }
        Random rand = new Random();
        String special = "+!@#$%^&*()_=";
        StringBuffer pass = new StringBuffer();
        int x = 0;

        for(int i = 0; i < length; i++)
        {
            x = rand.nextInt(26);
            if(i % 3 == 2)
            {
               pass.append(special.charAt(x % 13));
            }
            if(i % 3 == 1)
            {
                pass.append(x % 10);
            }
            if(i % 3 == 0)
            {
              x += 97;
              pass.append((char)x);
            }
        }
        return pass.toString();
    }

    public boolean isFiboBin(int n)
    {
        int count = 0;
        for(int i = 1; i < n; i++)
        {
            int fib = fib(i);
            String binstr = Integer.toBinaryString(fib);
            int bin = Integer.parseInt(binstr);

            if(n == fib + bin)
                return true;
        }
            return false;
    }

    public static int fib(int i)
    {
        if(i == 1)
            return 1;
        if(i == 2)
            return 1;

        return fib(i - 1) + fib(i - 2);
    }
}
