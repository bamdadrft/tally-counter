package sbu.cs;

import java.util.ArrayList;
import java.util.List;

public class
ExerciseLecture6 {

    public long calculateEvenSum(int[] arr) {
        long sum = 0;
        for(int i = 0; i < arr.length; i += 2)
        {
           sum += arr[i];
        }
        return sum;
    }

    public int[] reverseArray(int[] arr)
    {
        for(int i = 0; i < arr.length/2; i++)
        {
            int tmp = arr[i];
            arr[i] = arr[arr.length-(i+1)];
            arr[arr.length-(i+1)] = tmp;
        }
        return arr;
    }

    public double[][] matrixProduct(double[][] m1, double[][] m2) throws RuntimeException
    {
        int row1 = m1.length, col1 = m1[0].length, row2 = m2.length, col2 = m2[0].length;
        if(col1 != row2)
        {
            throw new RuntimeException();
        }
        double[][] prod = new double[row1][col2];
        for(int i = 0; i < row1; i++)
        {
            for(int j = 0; j < col2; j++)
            {
                for(int k = 0; k < col1; k++)
                {
                    prod[i][j] += m1[i][k] * m2[k][j];
                }
            }
        }
        return prod;
    }

    public List<List<String>> arrayToList(String[][] names)
    {
        List<List<String>> m = new ArrayList<>();
        for(int i = 0; i < names.length; i++)
        {
            ArrayList<String> n = new ArrayList<>();
            for(int j = 0; j < names[0].length; j++)
            {
                n.add(names[i][j]);
            }
            m.add(n);
        }
        return m;
    }

    public List<Integer> primeFactors(int n)
    {
        ArrayList<Integer> pfac = new ArrayList<>();
        for(int i = 2; n > 1; i++)
        {
            while (n % i == 0)
            {
                if(!pfac.contains(i))
                {
                    pfac.add(i);
                }
                n /= i;
            }
        }
        return pfac;
    }

    public List<String> extractWord(String line)
    {
        List<String> words = new ArrayList<>();

        line = line.replaceAll("[()*&^%$#@!,?<>+1234567890]", "");
        String[] tmp = line.split(" ");

        for(int i = 0; i < tmp.length; i++)
        {
            words.add(tmp[i]);
        }
        return words;
    }
}
