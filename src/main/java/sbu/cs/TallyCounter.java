package sbu.cs;

public class TallyCounter implements TallyCounterInterface {

    private int counter = 0;
    @Override
    public void count()
    {
        counter++;
        if(counter > 9999)
        {
            counter = 9999;
        }
    }

    @Override
    public int getValue()
    {
        return counter;
    }

    @Override
    public void setValue(int value) throws IllegalValueException
    {
        if(value < 0 || value > 9999)
        {
            throw new IllegalValueException();
        }
        counter = value;
    }
}
